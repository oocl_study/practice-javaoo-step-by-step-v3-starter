package ooss;

import java.util.*;

public class Teacher extends Person {

    private Set<Klass> klassSet = new HashSet<>();

    public Teacher(Integer id, String name, Integer age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        StringBuilder introduce =new StringBuilder();
        introduce.append(super.introduce() + " I am a teacher.");
        if(!klassSet.isEmpty()){
            introduce.append(" I teach Class ");
            Iterator<Klass> iterator = klassSet.iterator();
            while(iterator.hasNext()){
                introduce.append(iterator.next().getNumber());
                if(iterator.hasNext()) introduce.append(", ");
                else introduce.append(".");
            }
        }

        return introduce.toString();
    }

    public void assignTo(Klass klass){
        this.klassSet.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return klassSet.contains(klass);
    }

    public boolean isTeaching(Student tom) {
        return klassSet.contains(tom.getKlass());
    }

    @Override
    public void speakWhenLeaderSure(Integer classNumber, String leaderName) {
        System.out.println(String.format("I am %s, teacher of Class %d. I know %s become Leader.", this.name, classNumber, leaderName));
    }
}
