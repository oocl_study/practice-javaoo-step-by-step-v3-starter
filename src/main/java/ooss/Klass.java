package ooss;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Klass {


    private Integer number;

    private Student leader;

    private Set<Teacher> teacherSet = new HashSet<>();

    private Set<Student> studentSet = new HashSet<>();


    public Klass(Integer number) {
        this.number = number;
    }

    public Integer getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object obj) {
        return this.number == ((Klass) obj).number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public void assignLeader(Student king) {
        if (king.getKlass() == null || !king.getKlass().equals(this)) {
            System.out.println("It is not one of us.");
        } else {
            this.leader = king;
            notifyTeacherAndStudent();
        }
    }

    public void notifyTeacherAndStudent(){
        teacherSet.stream().forEach(teacher -> {
//            System.out.println(String.format("I am %s, teacher of Class %d. I know %s become Leader.", teacher.getName(), this.number, this.leader.getName()));
            teacher.speakWhenLeaderSure(this.number, this.leader.getName());
        });

        studentSet.stream().forEach(student -> {
//            System.out.println(String.format("I am %s, student of Class %d. I know %s become Leader.", student.getName(), this.number, this.leader.getName()));
            student.speakWhenLeaderSure(this.number, this.leader.getName());
        });
    }

    public boolean isLeader(Student tom) {
        return leader != null && leader.equals(tom);
    }

    public void attach(Person person) {
        if (person instanceof Teacher) {
            teacherSet.add((Teacher) person);
        } else if (person instanceof Student) {
            studentSet.add((Student) person);
        }
    }

    public Student getLeader() {
        return leader;
    }
}
