package ooss;

public class Student extends Person {

    private Klass klass;

    public Student(Integer id, String name, Integer age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        StringBuilder introduce = new StringBuilder();
        introduce.append(super.introduce() + " I am a student.");
        if(klass != null){
            if(klass.isLeader(this)) introduce.append(String.format(" I am the leader of class %d.", klass.getNumber()));
            else introduce.append(String.format(" I am in class %s.", klass.getNumber()));
        }

        return introduce.toString();
    }

    public void join(Klass klass){
        this.klass = klass;
    }

    public boolean isIn(Klass klass){
        return this.klass == null ? false : this.klass.equals(klass);
    }

    public Klass getKlass() {
        return klass;
    }

    @Override
    public void speakWhenLeaderSure(Integer classNumber, String leaderName) {
        System.out.println(String.format("I am %s, student of Class %d. I know %s become Leader.", this.name, classNumber, leaderName));
    }
}
