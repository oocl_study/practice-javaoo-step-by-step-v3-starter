package ooss;

import java.util.Objects;

public class Person {

    protected Integer id;

    protected String name;

    protected Integer age;

    public Person(Integer id, String name, Integer age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String introduce(){
        return String.format("My name is %s. I am %d years old.", this.name, this.age);
    }

    public void speakWhenLeaderSure(Integer classNumber, String leaderName){

    }

    @Override
    public boolean equals(Object obj) {
        return this.id == ((Person)obj).id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
