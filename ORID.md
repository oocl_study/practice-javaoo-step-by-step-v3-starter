O:
In the morning, we started with a code review, and then we learned about the relevant writing methods of Stream streams, and then did some related exercises. In the afternoon, We mainly learned about object-oriented features: encapsulation, inheritance, polymorphism, and then did some exercises about it.
R:
harvest-full
I:
Using stream can make the code more structured and easy to read. And using object-oriented thinking also can structure the entire project.
D:
In the following studies, i will better understand object-oriented thinking and use it more correctly.